package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

public class Game extends Screen {

	public static enum GameModeType {
		INFINITE
	}
	
	public static final float WORLD_TO_BOX = 0.01f;
	public static final int BOX_TO_WORLD = 100;
		
	public static final Vector2 PLATFORM_PADDING = new Vector2(3.5f, 1.5f);
	
	public static float CAM_OFFSET = 0;
	
	// Box2D world for physics
	private World world;

	// Debug renderer for Box2d
	private Box2DDebugRenderer debugRenderer;

	//The current game mode
	private GameMode gameMode;
	
	public Game(Main game, GameModeType type) {
		super(game, new GameAssets());
		world = new World(new Vector2(0, -15f), true);
		debugRenderer = new Box2DDebugRenderer(true, true, true, true, true, true);
	
		switch(type) {
		case INFINITE:
			gameMode = new InfiniteRunMode(game, this);
		default:
			break;
		}
		
		Button moveLeft = new Button("constant-moveLeft", this, 0.5f, 0.5f, (getTexture("moveLeft").getWidth() * 3f) * Game.WORLD_TO_BOX, getTexture("moveLeft").getHeight() * Game.WORLD_TO_BOX, getTexture("moveLeft"));
		addComponent(moveLeft);
		
		Button moveRight = new Button("constant-moveRight", this, 3.5f, 0.5f, (getTexture("moveRight").getWidth() * 3f) * Game.WORLD_TO_BOX, getTexture("moveRight").getHeight() * Game.WORLD_TO_BOX, getTexture("moveRight"));
		addComponent(moveRight);
		
		Button jump = new Button("jump", this, 12.5f, 0.5f, (getTexture("jump").getWidth() * 3f) * Game.WORLD_TO_BOX, getTexture("jump").getHeight() * Game.WORLD_TO_BOX, getTexture("jump"));
		addComponent(jump);
		
		EventProxy.setGame(this);
	}

	@Override
	public void render(SpriteBatch batch) {
		gameMode.render(batch);
	}

	@Override
	public void renderDebug(SpriteBatch batch) {
		debugRenderer.render(world, getMain().getDebugCamera().combined);
		gameMode.renderDebug(batch);
	}

	@Override
	public void tick(float delta) {
		super.tick(delta);
		world.step(1.0f / 60f, 6, 2);
		gameMode.tick(delta);

		moveCam();
	}
	
	public void moveCam() {
		float ly = getMain().getCamera().position.y;
		getMain().getCamera().position.set(Main.WIDTH / 2, (gameMode.player.getY() + Player.HEIGHT / 2) - Main.HEIGHT / 3, 0);
		getMain().getDebugCamera().position.set(Main.WIDTH / 2, (gameMode.player.getY() + Player.HEIGHT / 2) - Main.HEIGHT / 3, 0);
		CAM_OFFSET += getMain().getCamera().position.y - ly;
		if(CAM_OFFSET <= -PLATFORM_PADDING.y) {
			switch(gameMode.getID()) {
			case "Infinite Run":
				gameMode.generateLevel(getMain().getCamera().position.y - ((Main.HEIGHT / 3) * 1.5f) - Platform.HEIGHT);
				break;
			}
			CAM_OFFSET = 0;
		}
	}
	
	public GameMode getGameMode() {
		return gameMode;
	}

	public World getWorld() {
		return world;
	}

	@Override
	public Color getClearColor() {
		return gameMode.getClearColor();
	}

	@Override
	public String getID() {
		return "Game";
	}
}
