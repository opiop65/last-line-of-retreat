package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;

public abstract class Entity extends GameObject {

	protected Game game;
	protected Body body;
	protected Sprite sprite;
	private boolean killed;
	private float width, height;
	//True = left, False = right
	private boolean flip, moveDir;
	//Render engine to render the entity
	protected EntityRenderer entityRenderer;
	
	public Entity(Game game, Sprite sprite, Body body, float width, float height) {
		this.game = game;
		this.body = body;
		this.sprite = sprite;
		this.width = width;
		this.height = height;
		this.flip = false;
		body.setUserData(sprite);
	}

	public void setEntityRenderer(EntityRenderer renderer) {
		this.entityRenderer = renderer;
	}
	
	@Override
	public void render(SpriteBatch batch) {
		entityRenderer.render(batch);
	}
	
	@Override
	public void tick(float delta) {
		entityRenderer.tick(delta);
	}
	
	public Game getGame() {
		return game;
	}
	
	public InputProxy getInput() {
		return game.getMain().getInput();
	}
	
	public float getX() {
		return body.getPosition().x;
	}

	public float getY() {
		return body.getPosition().y;
	}
	
	public float getWidth() {
		return width;
	}
	
	public float getHeight() {
		return height;
	}
	
	public Body getBody() {
		return body;
	}
	
	public Sprite getSprite() {
		return sprite;
	}
	
	public void addPos(float x, float y) {
		body.setTransform((body.getPosition().x + x * Gdx.graphics.getDeltaTime()), (body.getPosition().y + y * Gdx.graphics.getDeltaTime()), body.getAngle());
	}
	
	public void setPos(float x, float y) {
		body.getPosition().set(x, y);
	}
	
	public void kill() {
		killed = true;
	}
	
	public boolean isKilled() {
		return killed;
	}
	
	public void flipRight() {
		flip = false;
	}
	
	public void flipLeft() {
		flip = true;
	}
	
	public void flip() {
		flip = !flip;
	}
	
	public boolean getFlip() {
		return flip;
	}
	
	public boolean getMoveDir() {
		return moveDir;
	}
	
	public void setMoveDir(boolean moveDir) {
		this.moveDir = moveDir;
	}
	
	public abstract float getPosModX();
	public abstract float getPosModY();
}
