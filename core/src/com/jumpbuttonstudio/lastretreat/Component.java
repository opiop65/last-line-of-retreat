package com.jumpbuttonstudio.lastretreat;

public abstract class Component extends GameObject {
	
	protected Screen screen;
	protected Listener listener;
	private String ID;
	
	public Component(String ID, Screen screen) {
		this.screen = screen;
		this.ID = ID;
	}
	
	public void addListener(Listener listener) {
		this.listener = listener;
	}
	
	public Listener getListener() {
		return listener;
	}
	
	public String getID() {
		return ID;
	}
} 
