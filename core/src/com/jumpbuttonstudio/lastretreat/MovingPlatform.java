package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;

public class MovingPlatform extends Platform {

	public MovingPlatform(Game game, Sprite sprite, Body body, Type type, float width) {
		super(game, sprite, body, type, width);
		 setMoveDir(MathUtils.randomBoolean());
	}
	
	@Override
	public void tick(float delta) {
		super.tick(delta);
		if((getX() + getWidth()) + 0.2 >= Main.WIDTH) setMoveDir(false);
		if(getX() <= 0) setMoveDir(true);
		
		getBody().setLinearVelocity(getMoveDir() ? 4 : -4, 0);
	}
}
