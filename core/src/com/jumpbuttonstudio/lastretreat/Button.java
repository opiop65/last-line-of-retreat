package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Button extends ClickableComponent {

	public static final float MAIN_BUTTON_WIDTH = 7f;
	public static final float MAIN_BUTTON_HEIGHT = 0.6f;
	
	private Texture texture;
	private boolean constantPress;
	
	public Button(String ID, Screen screen, float x, float y, Texture texture) {
		this(ID, screen, x, y, texture.getWidth(), texture.getHeight(), texture);
	}
	
	public Button(String ID, Screen screen, float x, float y, float width, float height, Texture texture) {
		super(ID, screen, screen.getInput(), x, y, width, height);
		this.texture = texture;
		if(ID.contains("constant")) constantPress = true;
	}
	
	@Override
	public void render(SpriteBatch batch) {
		batch.draw(texture, hitbox.x, hitbox.y, hitbox.width, hitbox.height);
	}

	@Override
	public void tick(float delta) {
		super.tick(delta);
		if(!constantPress) {
			if(Gdx.input.justTouched() && hitbox.contains(new Vector2(screen.getInput().getX(), Main.HEIGHT - screen.getInput().getY()))) clicked = true;
		} else {
			if(Gdx.input.isTouched() && hitbox.contains(new Vector2(screen.getInput().getX(), Main.HEIGHT - screen.getInput().getY()))) clicked = true;
		}
		if(clicked && listener != null) listener.perform();
	}
	
	public Vector2 getPos() {
		return new Vector2(hitbox.getX(), hitbox.getY());
	}
	
	public Vector2 getSize() {
		return new Vector2(hitbox.getWidth(), hitbox.getHeight());
	}
}
