package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;

public abstract class StaticEntity extends Entity {

	public StaticEntity(Game game, Sprite sprite, Body body, int renderType, float width, float height) {
		super(game, sprite, body, width, height);
		setEntityRenderer(new StaticEntityRenderer(this));
	}
}	
