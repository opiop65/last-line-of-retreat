package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public class Converter {

	public static Vector2 centerScreen(float w, float h) {
		float width = (w / 2);
		float height = (h / 2);
		float sWidth = Main.WIDTH / 2;
		float sHeight = Main.HEIGHT / 2;
		return new Vector2(sWidth - width, sHeight - height);
	}
	
	public static Vector2 centerScreen(Texture texture) {
		return centerScreen(texture.getWidth(), texture.getHeight());
	}
}
