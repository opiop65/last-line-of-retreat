package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class EventProxy {

	@SuppressWarnings("unused")
	private static Game game;

	public static void notify(Object obj, int event) {
		//Handle system wide events here 
		switch (event) {
		case Event.EXIT_GAME:
			Gdx.app.exit();
			break;
		}
		//Switch off into entity specific cases
		if (obj instanceof Player) {
			//Handle player events in this if statement
			Player player = (Player) obj;
			switch (event) {
			case Event.PLAYER_MOVE_RIGHT:
				player.body.setLinearVelocity(Player.MOVESPEED, player.body.getLinearVelocity().y);
				player.setMoveDir(false);
				break;
			case Event.PLAYER_MOVE_LEFT:
				player.body.setLinearVelocity(-Player.MOVESPEED, player.body.getLinearVelocity().y);
				player.setMoveDir(true);
				break;
			case Event.PLAYER_JUMP:
				Vector2 pos = player.body.getPosition();
				player.body.applyLinearImpulse(0, 10, pos.x + Player.WIDTH / 2, pos.y + Player.HEIGHT / 2, true);
				break;
			case Event.PLAYER_STOP_MOVE_RIGHT:
				player.body.setLinearVelocity(0, player.body.getLinearVelocity().y);
				break;
			case Event.PLAYER_STOP_MOVE_LEFT:
				player.body.setLinearVelocity(0, player.body.getLinearVelocity().y);
				break;
			}
		}
		//System.out.println("Event: " + event);
	}

	public static void setGame(Game game) {
		EventProxy.game = game;
	}
}
