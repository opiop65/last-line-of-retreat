package com.jumpbuttonstudio.lastretreat;

public class MainMenuAssets extends Assets {

	public void init() {
		put("MainMenu/background.png", "background");
		put("MainMenu/logo.png", "logo");
		put("MainMenu/highlight.png", "highlight");
		put("MainMenu/resume.png", "play");
		put("MainMenu/options.png", "options");
		put("MainMenu/exit.png", "exit");
	}
}
