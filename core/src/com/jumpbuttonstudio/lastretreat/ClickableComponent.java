package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class ClickableComponent extends Component implements Tickable {

	protected Rectangle hitbox;
	protected InputProxy input;
	protected boolean clicked;
	
	public ClickableComponent(String ID, Screen screen, InputProxy input, float x, float y, float width, float height) {
		super(ID, screen);
		this.input = input;
		this.hitbox = new Rectangle(x, y, width, height);
	}

	@Override
	public void tick(float delta) {
		if(input.touched()) {
			Vector2 pos = input.get();
			if(hitbox.contains(pos)) {
				clicked = true;
			}
		}
	}
	
	public boolean isClicked() {
		return clicked;
	}

	public void setClicked(boolean clicked) {
		this.clicked = clicked;
	}
}
