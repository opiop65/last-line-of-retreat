package com.jumpbuttonstudio.lastretreat;

import java.util.HashMap;

import com.badlogic.gdx.graphics.Texture;

public abstract class Assets {

	private HashMap<String, Texture> textures = new HashMap<String, Texture>();
	private HashMap<String, AnimationPackage> animations = new HashMap<String, AnimationPackage>();
	
	public Assets() {
		init();
	}
	
	public abstract void init();
	
	public void put(String path, String key) {
		if(!textures.containsKey(key)) {
			textures.put(key, TextureLoader.load(path));
		}
	}

	public void putAnimation(AnimationPackage anim, String key) {
		if(!animations.containsKey(key)) {
			animations.put(key, anim);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		if(textures.containsKey(key))
			return (T) textures.get(key);
		else if(animations.containsKey(key))
			return (T) animations.get(key);
		else 
			return null;
	}
}
