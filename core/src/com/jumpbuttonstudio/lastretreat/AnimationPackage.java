package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationPackage implements Tickable {

	private Animation animation;
	private float elapsedTime;

	private AnimationPackage(Animation animation) {
		this.animation = animation;
	}

	public static AnimationPackage load(String path, int size, float delay) {
		return new AnimationPackage(new Animation(delay, TextureLoader.loadRegions(path, size, size)[0]));
	}
	
	public static AnimationPackage loadFromAnimation(AnimationPackage anim, float delay, int... frames) {
		TextureRegion[] textures = new TextureRegion[frames.length];
		for(int i = 0; i < textures.length; i++) {
			textures[i] = anim.getTexture(frames[i]);
		}
		return new AnimationPackage(new Animation(delay, textures));
	}

	@Override
	public void tick(float delta) {
		elapsedTime += Gdx.graphics.getDeltaTime();
	}

	public TextureRegion getTexture() {
		return getTexture(true);
	}

	public TextureRegion getTexture(boolean looping) {
		return animation.getKeyFrame(elapsedTime, looping);
	}
	
	public TextureRegion getTexture(int frame) {
		return animation.getKeyFrames()[frame];
	}
}
