package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Main implements ApplicationListener {

	private Vector2 actualSize, virtualSize;

	private Screen currentScreen;
	private OrthographicCamera camera, debugCamera, uiCamera;
	private SpriteBatch batch, debugBatch, uiBatch;
	private Color clearColor;
	private InputProxy input;

	public static final int WIDTH = 16;
	public static final int HEIGHT = 9;
	
	@Override
	public void create() {
		this.virtualSize = new Vector2(WIDTH, HEIGHT);
		this.actualSize = new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		batch = new SpriteBatch();
		debugBatch = new SpriteBatch();
		uiBatch = new SpriteBatch();

		camera = new OrthographicCamera(16, 9);
		camera.setToOrtho(false, 16, 9);

		debugCamera = new OrthographicCamera(16, 9);
		debugCamera.setToOrtho(false, 16, 9);

		uiCamera = new OrthographicCamera(16, 9);
		uiCamera.setToOrtho(false, 16, 9);
		
		input = new InputProxy(this);

		setScreen(new MainMenu(this));
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		currentScreen.render(batch);
		batch.end();

		debugCamera.update();

		debugBatch.setProjectionMatrix(debugCamera.combined);
		debugBatch.begin();
		currentScreen.renderDebug(debugBatch);
		debugBatch.end();
		
		uiCamera.update();
		
		uiBatch.setProjectionMatrix(uiCamera.combined);
		uiBatch.begin();
		currentScreen.renderUI(uiBatch);
		uiBatch.end();
		
		tick(Gdx.graphics.getDeltaTime());
	}

	public void tick(float delta) {
		currentScreen.tick(delta);

		if (input.keyDown(Keys.ESCAPE))
			EventProxy.notify(null, Event.EXIT_GAME);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

	public void setScreen(Screen screen) {
		clearColor = screen.getClearColor();
		currentScreen = screen;
	}

	public OrthographicCamera getCamera() {
		return camera;
	}

	public OrthographicCamera getDebugCamera() {
		return debugCamera;
	}

	public Screen getCurrentScreen() {
		return currentScreen;
	}

	public InputProxy getInput() {
		return input;
	}

	public int getActualWidth() {
		return (int) actualSize.x;
	}

	public int getActualHeight() {
		return (int) actualSize.y;
	}

	public int getVirtualWidth() {
		return (int) virtualSize.x;
	}

	public int getVirtualHeight() {
		return (int) virtualSize.y;
	}

	public float getActualAspectRatio() {
		return actualSize.y / actualSize.x;
	}

	public float getVirtualAspectRatio() {
		return virtualSize.y / virtualSize.x;
	}

	public Vector2 toVirtual(int x, int y) {
		return new Vector2(toVirtualX(x), toVirtualY(y));
	}

	public Vector2 toActual(int x, int y) {
		return new Vector2(toActualX(x), toActualY(y));
	}

	public float toActualX(float x) {
		return (x * ((float) actualSize.x / virtualSize.x));
	}

	public float toActualY(float y) {
		return (y * ((float) actualSize.y / virtualSize.y));
	}

	public float toVirtualX(float x) {
		return (x * ((float) virtualSize.x / actualSize.x));
	}

	public float toVirtualY(float y) {
		return (y * ((float) virtualSize.y / actualSize.y));
	}
}
