package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;

public class Player extends AnimatedEntity {

	public static final int MOVESPEED = 12;
	public static float WIDTH = 3.4f;
	public static float HEIGHT = 1.2f;
	
	private boolean jumping;
	private AnimatedEntityRenderer renderer;
	
	public Player(Game game, Sprite sprite, Body body) {
		super(game, game.getAnimation("playerIdle"), sprite, body, WIDTH, HEIGHT);
		this.renderer = (AnimatedEntityRenderer) entityRenderer;
	}
	
	@Override
	public void render(SpriteBatch batch) {
		super.render(batch);
	}
	
	@Override
	public void tick(float delta) {
		super.tick(delta);
		
		//Handle key presses
		if(getInput().keyDown(Keys.D) || ((Button) game.getComponent("constant-moveRight")).isClicked()) {
			EventProxy.notify(this, Event.PLAYER_MOVE_RIGHT);
			renderer.setAnimation("playerRun");
			flipRight();
		}
		if(getInput().keyDown(Keys.A) || ((Button) game.getComponent("constant-moveLeft")).isClicked()) {
			EventProxy.notify(this, Event.PLAYER_MOVE_LEFT);
			renderer.setAnimation("playerRun");
			flipLeft();
		}
		if((getInput().keyJustPressed(Keys.SPACE) || ((Button) game.getComponent("jump")).isClicked()) && getBody().getLinearVelocity().y == 0) {
			EventProxy.notify(this, Event.PLAYER_JUMP);
			renderer.setAnimation("playerJump");
			jumping = true;
		}
		
		//Handle key releases
		if((!getInput().keyDown(Keys.D) && !getInput().keyDown(Keys.A)) && !((Button) game.getComponent("constant-moveRight")).isClicked()  && !((Button) game.getComponent("constant-moveLeft")).isClicked()) {
			EventProxy.notify(this, Event.PLAYER_STOP_MOVE_RIGHT);
			if(!jumping) renderer.setAnimation("playerIdle");
		}
		if(!getInput().keyDown(Keys.A) && !getInput().keyDown(Keys.D) && !((Button) game.getComponent("constant-moveLeft")).isClicked() && !((Button) game.getComponent("constant-moveRight")).isClicked()) { 
			EventProxy.notify(this, Event.PLAYER_STOP_MOVE_LEFT);
			if(!jumping) renderer.setAnimation("playerIdle");
		}
		
		//Handle falling and hitting a platform
		if(getBody().getLinearVelocity().y < 0.0f)
			renderer.setAnimation("playerFall");
		//See if player is on the ground (no y axis velocity)
		if(getBody().getLinearVelocity().y == 0) {
			renderer.setAnimation("playerIdle");
			jumping = false;
		}
	}

	@Override
	public float getPosModX() {
		return (getMoveDir() ? -0.3f : 0.3f);
	}

	@Override
	public float getPosModY() {
		return 0.18f;
	}
}
