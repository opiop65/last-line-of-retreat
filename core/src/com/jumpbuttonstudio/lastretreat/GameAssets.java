package com.jumpbuttonstudio.lastretreat;

public class GameAssets extends Assets {

	public void init() {
		put(Platform.Type.SMALL.getTexPath(), "smallP");
		put(Platform.Type.MEDIUM.getTexPath(), "mediumP");
		put(Platform.Type.LARGE.getTexPath(), "largeP");
		put(Platform.Type.XLARGE.getTexPath(), "xlargeP");
		put("Game/jump.png", "jump");
		put("Game/moveRight.png", "moveRight");
		put("Game/moveLeft.png", "moveLeft");
		putAnimation(AnimationPackage.load("sprites/player/idle.png", 37, 0.25f), "playerIdle");
		putAnimation(AnimationPackage.load("sprites/player/idle_shoot.png", 37, 0.1f), "playerIdleShoot");
		putAnimation(AnimationPackage.load("sprites/player/jump.png", 37, 0.1f), "playerJump");
		putAnimation(AnimationPackage.load("sprites/player/run.png", 37, 0.1f), "playerRun");
		putAnimation(AnimationPackage.load("sprites/player/run_shoot.png", 37, 0.1f), "playerRunShoot");
		putAnimation(AnimationPackage.loadFromAnimation((AnimationPackage) get("playerJump"), 0.1f, 2), "playerFall");
	}
}
