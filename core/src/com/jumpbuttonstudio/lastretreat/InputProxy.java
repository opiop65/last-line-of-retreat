package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class InputProxy {

	private final Main game;
	
	public InputProxy(Main game) {
		this.game = game;
	}
	
	public boolean touched(int i) {
		return Gdx.input.isTouched(i);
	}
	
	public boolean touched() {
		return touched(0);
	}
	
	public boolean justTouched() {
		return Gdx.input.justTouched();
	}
	
	public float getX(int i) {
		return game.toVirtualX(Gdx.input.getX(i));
	}
	
	public float getX() {
		return getX(0);
	}
	
	public float getY(int i) {
		return game.toVirtualY(Gdx.input.getY(i));
	}
	
	public float getY() {
		return getY(0);
	}
	
	public Vector2 get() {
		return new Vector2(getX(), getY());
	}
	
	public Vector2 get(int i) {
		return new Vector2(getX(i), getY(i));
	}
	
	public boolean keyDown(int key) {
		return Gdx.input.isKeyPressed(key);
	}
	
	public boolean keyJustPressed(int key) {
		return Gdx.input.isKeyJustPressed(key);
	}
}