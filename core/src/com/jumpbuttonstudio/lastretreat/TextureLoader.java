package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureLoader {

	public static Texture load(String path) {
		Texture texture = new Texture(path);
		texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		return texture;
	}
	
	public static TextureRegion[][] loadRegions(String path, int w, int h) {
		return TextureRegion.split(load(path), w, h);
	}
}
