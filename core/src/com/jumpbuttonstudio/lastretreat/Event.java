package com.jumpbuttonstudio.lastretreat;


public abstract class Event {

	public static final int EXIT_GAME = 0; 
	public static final int PLAYER_MOVE_RIGHT = 1;
	public static final int PLAYER_MOVE_LEFT = 2;
	public static final int PLAYER_STOP_MOVE_RIGHT = 5;
	public static final int PLAYER_STOP_MOVE_LEFT = 6;
	public static final int PLAYER_JUMP = 3;
}
