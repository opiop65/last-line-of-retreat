package com.jumpbuttonstudio.lastretreat;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;

public abstract class GameMode extends Screen {

	protected ArrayList<PlatformLevel> platformLevels = new ArrayList<PlatformLevel>();
	protected Player player;

	private Game game;

	public GameMode(Main main, Game game, Assets assets) {
		super(main, assets);
		this.game = game;
		enter();
	}

	public abstract void enter();

	public abstract void exit();

	// Generate a new level
	public void generateLevel(final float y) {
		// Temp list of platforms to put into platform level
		ArrayList<Platform> tempPlatforms = new ArrayList<Platform>();
		// Generate the first platform with maybe an offset

		tempPlatforms.add(generatePlatform(MathUtils.randomBoolean() ? MathUtils.random(0, 3) : 0, y, Platform.MoveType.STATIC));

		// integer to store the max x position
		float xOffset = tempPlatforms.get(0).getX() + tempPlatforms.get(0).getType().getWidth();
		while (xOffset < Main.WIDTH) {
			tempPlatforms.add(generatePlatform(xOffset + Game.PLATFORM_PADDING.x, y, Platform.MoveType.STATIC));
			xOffset = tempPlatforms.get(tempPlatforms.size() - 1).getX() + tempPlatforms.get(tempPlatforms.size() - 1).getType().getWidth();
		}

		platformLevels.add(new PlatformLevel(tempPlatforms));
	}

	private Platform generatePlatform(final float x, final float y, Platform.MoveType moveType) {
		// Start with a type (generate number between 1 and 11)
		// 1 - 4 = small, 5 - 8 = medium, 9 - 10 = large, 11 = xlarge
		final int tr = MathUtils.random(1, 11);

		Platform.Type type = null;
		if (tr >= 1 && tr <= 2)
			type = Platform.Type.SMALL;
		else if (tr >= 3 && tr <= 7)
			type = Platform.Type.MEDIUM;
		else if (tr >= 8 && tr <= 10)
			type = Platform.Type.LARGE;
		else if (tr >= 11 && tr <= 12)
			type = Platform.Type.XLARGE;

		return EntityFactory.getPlatform(getGame(), type, moveType, x + type.getWidth() / 2, y);

	}

	public void render(SpriteBatch batch) {
		for (PlatformLevel level : platformLevels) {
			level.render(batch);
		}
	}

	public void tick(float delta) {
		for (int i = 0; i < platformLevels.size(); i++) {
			if (platformLevels.get(i).getPlatforms().isEmpty())
				platformLevels.remove(i);
			else {
				platformLevels.get(i).tick(delta);
				if (platformLevels.get(i).getPlatforms().get(0).getBody().getPosition().y - Game.PLATFORM_PADDING.y > Main.HEIGHT + (Game.PLATFORM_PADDING.y * 4))
					platformLevels.remove(i);
			}
		}
	}

	public boolean levelInView(PlatformLevel l) {
		float y = l.getPlatforms().get(0).getBody().getPosition().y;
		return y + Game.PLATFORM_PADDING.y > 0 && y - Game.PLATFORM_PADDING.y <= Main.HEIGHT;
	}

	public void reset() {
		Array<Body> bodies = new Array<Body>();
		game.getWorld().getBodies(bodies);
		for (Body b : bodies)
			game.getWorld().destroyBody(b);
		platformLevels.clear();
	}

	public Game getGame() {
		return game;
	}

	public class PlatformLevel extends GameObject {
		private ArrayList<Platform> platforms;

		PlatformLevel(ArrayList<Platform> p) {
			this.platforms = p;
		}

		@Override
		public void render(SpriteBatch batch) {
			for (Platform p : platforms)
				p.render(batch);
		}

		@Override
		public void tick(float delta) {
			for (int i = 0; i < platforms.size(); i++) {
				if (platforms.get(i).isKilled())
					platforms.remove(i);
				else {
					platforms.get(i).tick(delta);
				}
			}
		}

		public void move(float x, float y) {
			for (Platform p : platforms)
				p.addPos(x, y);
		}

		public ArrayList<Platform> getPlatforms() {
			return platforms;
		}
	}
}
