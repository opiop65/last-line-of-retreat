package com.jumpbuttonstudio.lastretreat;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Screen extends GameObject {
	
	private Main main;
	private Assets assets;
	private ArrayList<Component> components;
	
	public Screen(Main main, Assets assets) {
		this.main = main;
		this.assets = assets;
		this.components = new ArrayList<Component>();
	}
	
	public void renderUI(SpriteBatch batch) {
		for(Component c : components)
			c.render(batch);
	}
	
	@Override
	public void tick(float delta) {
		for(Component c : components)
			c.tick(delta);
		if(!Gdx.input.isTouched()) {
			for(Component c : components) {
				if(c instanceof ClickableComponent) ((ClickableComponent) c).setClicked(false);
			}
		}
	}
	
	public void addComponent(Component c) {
		components.add(c);
	}
	
	public Component getComponent(String ID) {
		for(Component c : components)
			if(c.getID().equals(ID)) return c;
		return null;
	}
	
	public InputProxy getInput() {
		return main.getInput();
	}
	
  	public Main getMain() {
		return main;
	}
	
	public Texture getTexture(String key) {
		return assets.get(key);
	}
	
	public AnimationPackage getAnimation(String key) {
		return assets.get(key);
	}
	
	public abstract void renderDebug(SpriteBatch batch);
	public abstract Color getClearColor();
	public abstract String getID();
}
