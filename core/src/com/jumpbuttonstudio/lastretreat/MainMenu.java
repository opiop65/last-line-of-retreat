package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jumpbuttonstudio.lastretreat.Game.GameModeType;

public class MainMenu extends Screen {

	private ButtonList buttons;
	
	public MainMenu(Main game) {
		super(game, new MainMenuAssets());
		
		Button play = new Button("play", this, Converter.centerScreen(Button.MAIN_BUTTON_WIDTH, Button.MAIN_BUTTON_HEIGHT).x, 2.7f, Button.MAIN_BUTTON_WIDTH, Button.MAIN_BUTTON_HEIGHT, getTexture("play"));
		play.addListener(new Listener() {
			@Override
			public void perform() {
				getMain().setScreen(new Game(getMain(), GameModeType.INFINITE));
			}
		});
		
		Button options = new Button("options", this, Converter.centerScreen(Button.MAIN_BUTTON_WIDTH, Button.MAIN_BUTTON_HEIGHT).x, 1.8f, Button.MAIN_BUTTON_WIDTH, Button.MAIN_BUTTON_HEIGHT, getTexture("options"));
		
		Button exit = new Button("exit", this, Converter.centerScreen(Button.MAIN_BUTTON_WIDTH, Button.MAIN_BUTTON_HEIGHT).x, 0.9f, Button.MAIN_BUTTON_WIDTH, Button.MAIN_BUTTON_HEIGHT, getTexture("exit"));
		exit.addListener(new Listener() {
			@Override
			public void perform() {
				Gdx.app.exit();
			}
		});
		
		buttons = new ButtonList("MainMenu_buttons", this, play, options, exit);
		addComponent(buttons);
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.draw(getTexture("background"), 0, 0, Main.WIDTH, Main.HEIGHT);
		batch.draw(getTexture("logo"), (Main.WIDTH / 2) - (((getTexture("logo").getWidth() * 4) / 2) * Game.WORLD_TO_BOX), (Main.HEIGHT - (getTexture("logo").getHeight() * Game.WORLD_TO_BOX)) - 0.5f, (getTexture("logo").getWidth() * Game.WORLD_TO_BOX) * 4, getTexture("logo").getHeight() * Game.WORLD_TO_BOX);
	}

	@Override
	public void renderDebug(SpriteBatch batch) {
	}
	
	@Override
	public void tick(float delta) {
		buttons.tick(delta);
	}

	@Override
	public Color getClearColor() {
		return Color.BLACK;
	}

	@Override
	public String getID() {
		return "Main Menu";
	}
}
