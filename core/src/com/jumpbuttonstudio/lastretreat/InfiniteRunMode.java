package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class InfiniteRunMode extends GameMode {

	//Amount of padding to put between the player's head and the top of the screen
	public static final float PLAYER_TOP_PADDING = Player.HEIGHT + (1);
	
	public InfiniteRunMode(Main main, Game game) {
		super(main, game, new InfiniteRunAssets());
	}

	@Override
	public void enter() {
		for(int i = 1; i < 6; i++) { 
			generateLevel(Main.HEIGHT - (i * Game.PLATFORM_PADDING.y));
		}
		player = EntityFactory.getPlayer(getGame(), Main.WIDTH / 2, Main.HEIGHT);
	}

	@Override
	public void exit() {
		reset();
	}

	@Override
	public void render(SpriteBatch batch) {
		super.render(batch);
		player.render(batch);
	}

	@Override
	public void tick(float delta) {
		super.tick(delta);
		player.tick(delta);
	}

	@Override
	public void renderDebug(SpriteBatch batch) {
	}

	@Override
	public Color getClearColor() {
		return Color.TEAL;
	}

	@Override
	public String getID() {
		return "Infinite Run";
	}
}
