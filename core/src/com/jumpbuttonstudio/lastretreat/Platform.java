package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;

public class Platform extends StaticEntity {

	public static enum Type {
		SMALL(1f * 2, "platforms/platform4.png", "smallP"), MEDIUM(1.4f * 2, "platforms/platform3.png", "mediumP"), LARGE(2.55f * 2, "platforms/platform2.png", "largeP"), XLARGE(4 * 2, "platforms/platform1.png", "xlargeP");
		
		float width;
		String texPath, tex;
		
		Type(float width, String texPath, String tex) {
			this.width = width;
			this.texPath = texPath;
			this.tex = tex;
		}
		
		public String getTexPath() {
			return texPath;
		}
		
		public String getTex() {
			return tex;
		}
		
		public float getWidth() {
			return width;
		}
	}
	
	public static enum MoveType {
		STATIC, MOVING
	}
	
	public static MoveType getMoveType() {
		float r = MathUtils.random(1.0f);
		if(r >= 0.0f && r <= 0.7f) return MoveType.STATIC;
		else return MoveType.MOVING;
	}
	
	public static final float HEIGHT = 0.2f;
	
	private Type type;
	
	public Platform(Game game, Sprite sprite, Body body, Type type, float width) {
		super(game, sprite, body, EntityRenderer.STATIC, width, HEIGHT);
		this.type = type;
	}
	
	@Override
	public void tick(float delta) {
		super.tick(delta);
	}
	
	public Type getType() {
		return type;
	}

	@Override
	public float getPosModX() {
		return 0;
	}

	@Override
	public float getPosModY() {
		return 0;
	}
}
