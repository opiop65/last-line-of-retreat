package com.jumpbuttonstudio.lastretreat;

public interface Tickable {
	public void tick(float delta);
}
