package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AnimatedEntityRenderer extends EntityRenderer {

	private AnimationPackage currentAnimation;
	
	public AnimatedEntityRenderer(Entity entity, AnimationPackage animation) {
		super(entity);
		this.currentAnimation = animation;
	}

	@Override
	public void render(SpriteBatch batch) {
		Sprite sprite = (Sprite) entity.getBody().getUserData();
		sprite.setRegion(currentAnimation.getTexture());
		sprite.setFlip(entity.getFlip(), false);
		sprite.setPosition((entity.getX() - (entity.getWidth() / 2)) + entity.getPosModX(), (entity.getY() - (entity.getHeight() / 2)) + entity.getPosModY());
		sprite.setSize(entity.getWidth(), entity.getHeight());
		sprite.draw(batch);
	}

	@Override
	public void tick(float delta) {
		currentAnimation.tick(delta);
	}
	
	public void setAnimation(String animation) {
		this.currentAnimation = entity.getGame().getAnimation(animation);
	}
	
	public AnimationPackage getAnimation() {
		return currentAnimation;
	}
}
