package com.jumpbuttonstudio.lastretreat;

public abstract class Listener {
	public abstract void perform();
}
