package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;

public class Obstacle extends Entity {

	public Obstacle(Game game, Sprite sprite, Body body, float width, float height) {
		super(game, sprite, body, width, height);
	}

	@Override
	public float getPosModX() {
		return 0;
	}

	@Override
	public float getPosModY() {
		return 0;
	}
}
