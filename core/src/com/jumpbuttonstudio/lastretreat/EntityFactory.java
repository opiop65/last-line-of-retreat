package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class EntityFactory {

	public static Platform getPlatform(Game game, Platform.Type type, Platform.MoveType moveType, float x, float y) {
		switch(moveType) {
		case STATIC:
			return new Platform(game, new Sprite(game.getTexture(type.getTex())), getKinematicBody(game.getWorld(), x, y, type.getWidth(), Platform.HEIGHT), type, type.getWidth());
		case MOVING:
			return new MovingPlatform(game, new Sprite(game.getTexture(type.getTex())), getKinematicBody(game.getWorld(), x, y, type.getWidth(), Platform.HEIGHT), type, type.getWidth());
		default:
			return new Platform(game, new Sprite(game.getTexture(type.getTex())), getKinematicBody(game.getWorld(), x, y, type.getWidth(), Platform.HEIGHT), type, type.getWidth());
		}
	}

	public static Player getPlayer(Game game, float x, float y) {
		return new Player(game, new Sprite(), getDynamicBody(game.getWorld(), x, y, Player.WIDTH - 2f, Player.HEIGHT - 0.4f));
	}
	
	public static Body getDynamicBody(World world, float x, float y, float width, float height) {
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		def.position.set(x, y);
		
		Body body = world.createBody(def);
		body.setFixedRotation(true);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(width / 2, height / 2);
		
		FixtureDef fixture = new FixtureDef();
		fixture.shape = shape;
		fixture.density = 1;
		fixture.friction = 0;

		body.createFixture(fixture);
		
		shape.dispose();
		
		return body;
	}
	
	public static Body getStaticBody(World world, float x, float y, float width, float height) {
		BodyDef def = new BodyDef();
		def.position.set(x, y);

		Body body = world.createBody(def);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(width / 2, height / 2);
		body.createFixture(shape, 0.0f);
		
		FixtureDef fixture = new FixtureDef();
		fixture.shape = shape;
		fixture.density = 1;
		fixture.friction = 0;

		body.createFixture(fixture);
		
		shape.dispose();
		
		return body;
	}
	
	public static Body getKinematicBody(World world, float x, float y, float width, float height) {
		BodyDef def = new BodyDef();
		def.type = BodyType.KinematicBody;
		def.position.set(x, y);

		Body body = world.createBody(def);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(width / 2, height / 2);
		body.createFixture(shape, 0.0f);
		
		FixtureDef fixture = new FixtureDef();
		fixture.shape = shape;
		fixture.density = 1;
		fixture.friction = 0;

		body.createFixture(fixture);
		
		shape.dispose();
		
		return body;
	}
}
