package com.jumpbuttonstudio.lastretreat;

public abstract class EntityRenderer implements Renderable, Tickable {

	public static final int STATIC = 0;
	public static final int ANIMATED = 1;
	
	protected Entity entity;
	
	public EntityRenderer(Entity entity) {
		this.entity = entity;
	}
}
