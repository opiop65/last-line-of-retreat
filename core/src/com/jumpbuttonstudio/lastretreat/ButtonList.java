package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ButtonList extends Component {

	private Button[] buttons;
	private int currentButton = 0;
	
	public ButtonList(String ID, Screen screen, Button... buttons) {
		super(ID, screen);
		this.buttons = buttons;
	}
	
	@Override
	public void render(SpriteBatch batch) {
		batch.draw(screen.getTexture("highlight"), 0, buttons[currentButton].getPos().y - 0.1f, Main.WIDTH / 2 + 4, buttons[currentButton].getSize().y + 0.2f);
		for(Button b : buttons)
			b.render(batch);
	}

	@Override
	public void tick(float delta) {
		for(Button b : buttons)
			b.setClicked(false);
		if(screen.getInput().keyJustPressed(Keys.DOWN)) {
			if(currentButton + 1 > buttons.length - 1) currentButton = 0;
			else currentButton++;
		} else if(screen.getInput().keyJustPressed(Keys.UP)) {
			if(currentButton - 1 < 0) currentButton = buttons.length - 1;
			else currentButton--;
		} 
		if(screen.getInput().keyJustPressed(Keys.ENTER)) {
			buttons[currentButton].setClicked(true);
		}
		for(int i = 0; i < buttons.length; i++) {
			buttons[i].tick(delta);
			if(buttons[i].isClicked()) currentButton = i;
		}
	}
}
