package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StaticEntityRenderer extends EntityRenderer {

	public StaticEntityRenderer(Entity entity) {
		super(entity);
	}

	@Override
	public void render(SpriteBatch batch) {
		Sprite sprite = (Sprite) entity.getBody().getUserData();
		sprite.setSize(entity.getFlip() ? -entity.getWidth() : entity.getWidth(), entity.getHeight());
		sprite.setPosition((entity.getX() - (entity.getWidth() / 2)) + entity.getPosModX(), (entity.getY() - (entity.getHeight() / 2)) + entity.getPosModY());
		sprite.draw(batch);
	}

	@Override
	public void tick(float delta) {
	}
}
