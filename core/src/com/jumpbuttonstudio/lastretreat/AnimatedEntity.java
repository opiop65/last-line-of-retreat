package com.jumpbuttonstudio.lastretreat;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;

public abstract class AnimatedEntity extends Entity {
	
	public AnimatedEntity(Game game, AnimationPackage animation, Sprite sprite, Body body, float width, float height) {
		super(game, sprite, body, width, height);
		setEntityRenderer(new AnimatedEntityRenderer(this, animation));
	}
}
